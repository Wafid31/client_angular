import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {InfirmierInterface} from "../dataInterfaces/nurse";
import {Adresse} from "../dataInterfaces/adress";

@Component({
  selector: 'app-infirmier',
  templateUrl: './infirmier.component.html',
  styleUrls: ['./infirmier.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class InfirmierComponent implements OnInit {

  @Input("data") private infirmier: InfirmierInterface;
  private http: any;

  constructor() { }

  ngOnInit() {
  }

  getFullName(): string {
    return `${this.infirmier.prénom} ${this.infirmier.nom}`;
  }

  getPhoto(): string {
    return `${this.infirmier.photo}`;
  }

  getAdresse(): string {
    return `${this.infirmier.adresse.numéro} ${this.infirmier.adresse.rue} ${this.infirmier.adresse.ville} ${this.infirmier.adresse.codePostal}`;
  }

  getId() {
    return `${this.infirmier.id}`;
  }
}
