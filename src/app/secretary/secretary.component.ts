import { Component, OnInit } from '@angular/core';
import {CabinetMedicalService} from '../cabinet-medical.service';
import {Http} from '@angular/http';
import {InfirmierInterface} from "../dataInterfaces/nurse";
import {CabinetInterface} from "../dataInterfaces/cabinet";
import {PatientInterface} from "../dataInterfaces/patient";
import {sexeEnum} from "../dataInterfaces/sexe";
import {Adresse} from "../dataInterfaces/adress";

@Component({
  selector: 'app-secretary',
  templateUrl: './secretary.component.html',
  styleUrls: ['./secretary.component.css']
})
export class SecretaryComponent implements OnInit {
  private cabinet: CabinetInterface = {
    patientsNonAffectés: [],
    infirmiers: [],
    adresse: undefined
  };

  constructor(private cabinetService: CabinetMedicalService) {
    cabinetService.getData('/data/cabinetInfirmier.xml').then(
      cabinet => this.cabinet = cabinet
    );
  }
  ngOnInit() {
  }

  getInfirmiers(): InfirmierInterface[] {
    return this.cabinet.infirmiers;
  }

  getPatients(): PatientInterface[] {
    return this.cabinet ? this.cabinet.patientsNonAffectés : [] ;
  }
  getPatientsNF(): PatientInterface[] {
    return this.cabinet.patientsNonAffectés ;
  }

  createPatient(name: string, prenom: string , sexe: sexeEnum, numSecu: string, etage: string, numRue: string, rue: string, codeP: number, ville: string) {
    if (name && prenom && sexe && numSecu && etage && numRue && rue && codeP && ville) {
      console.log("ajout d un client ");
    const ad: Adresse = {
      numéro: numRue,
        rue: rue,
        codePostal: codeP,
        ville: ville,
        étage: etage,
        lat: undefined,
        lng: undefined
    };
    const patient: PatientInterface = {
      nom: name,
      prénom: prenom,
      numéroSécuritéSociale: numSecu,
      sexe: sexe,
      adresse: ad
    };
    this.cabinetService.addPatient(patient);
    this.cabinet.patientsNonAffectés.push(patient);}
  }

  affecterPatient(infirmierId, patient) {
    this.cabinetService.affecterPatient(infirmierId, patient);
    for (let i = 0; i < this.cabinet.infirmiers.length; i++) {
      if (this.cabinet.infirmiers[i].id === infirmierId) {
        this.cabinet.infirmiers[i].patients.push(patient);
        const p = this.cabinet.patientsNonAffectés.indexOf(patient);
        this.cabinet.patientsNonAffectés.splice(p, 1);
      }
    }
  }

  desaffecterPatient(infirmierId, patient) {
    this.cabinetService.desaffecterPatient(infirmierId, patient);
    for (let i = 0; i < this.cabinet.infirmiers.length; i++) {
      if (this.cabinet.infirmiers[i].id === infirmierId) {
        const pp = this.cabinet.infirmiers[i].patients.indexOf(patient);
        this.cabinet.infirmiers[i].patients.splice(pp, 1);
        this.cabinet.patientsNonAffectés.push(patient);
      } }
  }

  SuppPatient(patient){
    const pp = this.cabinet.patientsNonAffectés.indexOf(patient);
    this.cabinet.patientsNonAffectés.splice(pp, 1);  }

    DoSomethingWhenDrop() {
    console.log("Got it ::::");
    }
}
