"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@angular/core");
const http_1 = require("@angular/http");
let CabinetMedicalService = class CabinetMedicalService {
    //  infirmiers: InfirmierInterface[];
    // patients: PatientInterface[];
    constructor(http) {
        this.http = http;
    }
    // télécharge étét de la base dans le serveur
    // a une dépendance au service http por faire des requêtes
    getData(url) {
        return this.http.get(url).toPromise().then((response) => {
            const text = response.text();
            console.log(text);
            const parser = new DOMParser();
            const doc = parser.parseFromString(text, 'text/xml');
            console.log(doc);
            const cabinet = {
                infirmiers: [],
                patientsNonAffectés: [],
                adresse: null,
            };
            const infirmiersXML = Array.from(doc.querySelectorAll('infirmiers>infirmier'));
            cabinet.infirmiers = infirmiersXML.map(infXML => ({
                id: infXML.getAttribute('id'),
                prénom: infXML.querySelector('prénom').textContent,
                nom: infXML.querySelector('nom').textContent,
                photo: infXML.querySelector('photo').textContent,
                patients: [],
                adresse: this.getAdresseFrom(infXML.querySelector('adresse'))
            }));
            console.log(infirmiersXML);
            return null;
        });
    }
    getAdresseFrom(adresseXML) {
        // const adresses = Array.from(doc.querySelectorAll('infirmiers>infirmier>adresse'));
        return {
            numéro: adresseXML.querySelector('numéro').textContent,
            rue: adresseXML.querySelector('rue').textContent,
            ville: adresseXML.querySelector('ville').textContent,
            codePostal: parseInt(adresseXML.querySelector('codePostal').textContent, 10)
        };
    }
};
CabinetMedicalService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], CabinetMedicalService);
exports.CabinetMedicalService = CabinetMedicalService;
//# sourceMappingURL=cabinet-medical.service.js.map