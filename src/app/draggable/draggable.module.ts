import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OverlayModule } from '@angular/cdk/overlay';

import { DraggableDirective } from './draggable.directive';
import { MovableDirective } from './movable.directive';
import { SortableDirective } from './sortable.directive';
import { SortableListDirective } from './sortable-list.directive';

@NgModule({
  imports: [
    CommonModule,
    OverlayModule
  ],
  declarations: [
    DraggableDirective,
    MovableDirective,
    SortableDirective,
    SortableListDirective
  ],
  exports: [
    DraggableDirective,
    MovableDirective,
    SortableListDirective,
    SortableDirective
  ]
})
export class DraggableModule { }
