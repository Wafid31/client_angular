import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {PatientInterface} from "../dataInterfaces/patient";
import {CabinetMedicalService} from "../cabinet-medical.service";

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class PatientComponent implements OnInit {
  @Input("data") private patient: PatientInterface;
  private cabinetService: CabinetMedicalService;
  constructor() {

  }

  ngOnInit() {
  }

  getFullName(): string {
    return `${this.patient.nom} ${this.patient.prénom}`;
  }

  getNumsecu(): string {
    return`${this.patient.numéroSécuritéSociale}`;
  }

  getAdresse(): string {
    return `${this.patient.adresse.numéro} ${this.patient.adresse.rue} ${this.patient.adresse.ville} ${this.patient.adresse.codePostal}`;
  }



}
