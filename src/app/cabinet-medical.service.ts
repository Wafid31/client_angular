import { Injectable } from '@angular/core';
import {CabinetInterface} from './dataInterfaces/cabinet';
import {Http, Response} from '@angular/http';
import {InfirmierInterface} from './dataInterfaces/nurse';
import {PatientInterface} from './dataInterfaces/patient';
import {Adresse} from './dataInterfaces/adress';
import {sexeEnum} from "./dataInterfaces/sexe";

  @Injectable()
export class CabinetMedicalService {
  constructor(private _http: Http) { }
  getData( url: string ): Promise<CabinetInterface> {
  return  this._http.get(url).toPromise().then(
      (response: Response ) => {
        const text = response.text();
        console.log(text);
        const parser = new DOMParser();
        const doc = parser.parseFromString(text, 'text/xml');
        console.log(doc);
        const cabinet: CabinetInterface = {
          infirmiers : [],
          patientsNonAffectés : [],
          adresse : null,
        };

        const patientsXML = Array.from(doc.querySelectorAll('patients>patient'));
        const tableauToutPatients: PatientInterface[] = patientsXML.map( patXML => this.getPatientFrom(patXML) );

        const infirmiersXML = Array.from(doc.querySelectorAll('infirmiers>infirmier'));
        cabinet.infirmiers = infirmiersXML.map(infXML => ({
          id: infXML.getAttribute('id'),
          prénom: infXML.querySelector('prénom').textContent,
          nom: infXML.querySelector('nom').textContent,
          photo: infXML.querySelector('photo').textContent,
          patients:  [],
          adresse: this.getAdresseFrom(infXML.querySelector('adresse'))
          })
        );

        // calcule des affectations à réaliser
        const affectations: { pat: PatientInterface, inf: InfirmierInterface}[] = patientsXML.map(patientXML => {
          const numSécu = patientXML.querySelector('numéro').textContent;
          const patient = tableauToutPatients.find(p => p.numéroSécuritéSociale === numSécu);
          const idInf = patientXML.querySelector('visite').getAttribute('intervenant');
          const inf = cabinet.infirmiers.find(i => i.id === idInf);
          return{pat: patient , inf: inf};
        });

        // Réaliser affectations
        affectations.forEach( affectation => {
            if (affectation.inf ) {
              affectation.inf.patients.push(affectation.pat);
            } else {
              cabinet.patientsNonAffectés.push(affectation.pat);
          }
        });

        console.log(tableauToutPatients);
        console.log(cabinet);
        return cabinet;
      }
    );
  }

  getAdresseFrom(adresseXML: Element): Adresse {
    // const adresses = Array.from(doc.querySelectorAll('infirmiers>infirmier>adresse'));
    // passer la verification en ternaire
    if (!(adresseXML.querySelector('numéro'))) {
      return <Adresse>{
        rue: adresseXML.querySelector('rue').textContent,
        ville: adresseXML.querySelector('ville').textContent,
        codePostal: parseInt(adresseXML.querySelector('codePostal').textContent, 10)
      };
    }
    return <Adresse>{
      numéro: adresseXML.querySelector('numéro').textContent,
      rue: adresseXML.querySelector('rue').textContent,
      ville: adresseXML.querySelector('ville').textContent,
      codePostal: parseInt(adresseXML.querySelector('codePostal').textContent, 10)
    };
  }
  getPatientFrom(patientXML: Element): PatientInterface {
    return <PatientInterface> {
      prénom: patientXML.querySelector('prénom').textContent,
      nom: patientXML.querySelector('nom').textContent,
      sexe: patientXML.querySelector('sexe').textContent === 'M' ? sexeEnum.M : sexeEnum.F,
      numéroSécuritéSociale: patientXML.querySelector('numéro').textContent,
      adresse: this.getAdresseFrom(patientXML.querySelector('adresse'))
    };
  }

  addPatient(patient: PatientInterface) {
    this._http.post("/addPatient", {
      patientName: patient.nom,
      patientForname: patient.prénom,
      patientNumber: patient.numéroSécuritéSociale,
      patientSex: patient.sexe === sexeEnum.M ? "M" : "F",
      patientBirthday: "AAAA-MM-JJ",
      patientFloor: patient.adresse.étage,
      patientStreetNumber: patient.adresse.numéro,
      patientStreet: patient.adresse.rue,
      patientPostalCode: patient.adresse.codePostal,
      patientCity: patient.adresse.ville
    }).subscribe();
  }


    affecterPatient(infirmierId, patient) {
      this._http.post( "/affectation", {
        infirmier: infirmierId,
        patient: patient.numéroSécuritéSociale
      }).subscribe(
      );
    }
    desaffecterPatient(infirmier, patient) {
      this._http.post( "/affectation", {
        infirmier: "none",
      patient: patient.numéroSécuritéSociale
    });

    }

    SuppPatient(patient) {
      this._http.post( "/affectation", {
        patient: patient.numéroSécuritéSociale
      });

    }
}
