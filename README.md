Projet : YAMINI & HASDI
-Shema generale de notre Projet:(fichier construit par notpad++)

				|-----------------------------|                    |------------|
                |     Cabinet medical(Project)|-----Affichage----->| client     |
                |-----------------------------|	                   |------------|
                            ^   ^                           
                           /     \                          
						  /		Fichier cabinet
						 /			XML
                        /            \            
                       /			  \			
                      /            |----------------|         
                     /             |  recuperation  |
					/		       |	données     |
	      Angular material         |			    |	
			drag&drop	           |   (serveur)    |
                 /                 |----------------|            
                /                                         
               /                                           
    |----------------|                                 
    | representation |
	|	  de		 | 	
	|  l'interface   | 
    |----------------|                                  

-Explication :
	1)Partie Serveur :
		-on a pas des midifications à faire à part le mettre en ecoute grace à npm start
	2)Partie Client :
		2.1)Recuperation de données :
				-Le travail majeur est fait sur le fichier cabinet-medical-service.ts ou' on a parsé 
				 le fichier cabinet.xml et recupéré tout les informations (patients,infermier,adresse..)
				-on a affecter chaque patient à un infirmier responsable de lui 
				-et finalement une fonction qui permet d'ajouter un patient en tableau d'un infermier
		2.2)Affichage de la Secretary:
				-Premaration des methodes de manipulation patients,inferimiers,ajout,affectation,supression ..
				 dans le fichier secretary.component.ts
				-Affichage en utilisant les Angular materials and drag&drop dans le fichier secretary.component.html
		2.3)Les autres affichage :
				-Affichage des patient grace à patient.component.ts|html
				-Affichage des Infirmier grace à Infirmier.component.ts|html
		
		3)Draggable Files :
				-on a utilisé draggable.directive afin de detecter les deplacement de la souris et crée l'evenement apres chaque click
				-on a utilisé movable.directive pour pouvoir deplacer les patient qui sont afficher sous forme d'une liste (quand on lache sa revient automatiquement)
				-on a utilisé sortable|liste.directive pour mettre en ordre les element de la liste des patients ou la liste des infirmiers en les deplaçant
				
